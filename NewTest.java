package com.tstng.TestNgProject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.Duration;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Ignore;
import org.testng.annotations.Test;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class NewTest {

    WebDriver driver = null;

    @BeforeClass
    public void launchApplications() {
        driver = new ChromeDriver();
        driver.get("https://admin-demo.nopcommerce.com");
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));

    }

    @Test
    public void testdata1() {

        driver.findElement(By.xpath("//button[@type='submit']")).click();

        String text = driver.findElement(By.partialLinkText("John Smith")).getText();
        Assert.assertEquals(text, "John Smith");
    }

    @Test

    public void testdata2() throws InterruptedException, BiffException, IOException {
        driver.findElement(By.partialLinkText("Catalog")).click();
        Thread.sleep(2000);
        driver.findElement(By.partialLinkText("Categories")).click();
        driver.findElement(By.partialLinkText("Add new")).click();
    	File f=new File("C:\\Users\\D E L L\\Documents\\nopcommerce.xls");
        FileInputStream fis= new FileInputStream(f);

        Workbook book=Workbook.getWorkbook(fis);
        Sheet sh=book.getSheet("Sheet1");

        int rowCount= sh.getRows();
        int columnsCount=sh.getColumns();
        for(int i=1; i<rowCount; i++)
        {
            String name= sh.getCell(0,1).getContents();
            Thread.sleep(3000);
            String description= sh.getCell(1,1).getContents();
            driver.findElement(By.id("Name")).sendKeys(name);
            
            driver.switchTo().frame(0);
            driver.findElement(By.id("tinymce")).sendKeys(description);
            driver.switchTo().defaultContent();
            
            driver.findElement(By.xpath("//button[@name='save']")).click();
    	

       /* driver.findElement(By.name("Name")).sendKeys("Build your own computer");
        Thread.sleep(3000);
        driver.switchTo().frame(0);
        driver.findElement(By.id("tinymce")).sendKeys("This is the best computer"); */

        driver.switchTo().defaultContent();

        WebElement findElement = driver.findElement(By.id("ParentCategoryId"));
        Select s = new Select(findElement);
        s.selectByIndex(2);
        driver.findElement(By.name("save")).click();
        driver.findElement(By.xpath("//div[text()='Search']"));
        driver.findElement(By.name("SearchCategoryName")).sendKeys("Build your own computer");
        driver.findElement(By.id("search-categories")).click();
        String text = driver.findElement(By.xpath("//input[@name='checkbox_categories'][@value=\"23\"]")).getText();

    }}

    @Test
    public void testdata3() {
        driver.findElement(By.xpath("//p[normalize-space()='Products']")).click();
        driver.findElement(By.id("SearchProductName")).sendKeys("Apple MacBook Pro 13-inch");
        driver.findElement(By.id("search-products")).click();

    }
     
    @Ignore
    @Test
    public void testData4() {
        driver.findElement(By.xpath("(//p[normalize-space()='Manufacturers'])[1]")).click();
        driver.findElement(By.id("SearchManufacturerName")).sendKeys("HP");
        driver.findElement(By.id("search")).click();

    }

    @AfterClass
    public void close() {
        driver.close();
    }

}